
	
	; /** inspired from @thanks @see https://stackoverflow.com/questions/52760509/typescript-returntype-of-overloaded-function/52761156#52761156 original
		* @note n is atm capped at 8
		* @result [n] overload 1
		* @result [n-1] overload 2
		* @result [n-2] overload 3
		* @result ....
		* @result [3] overload n-2
		* @result [2] overload n-1
		* @result [1] overload n
		* @result [0] overloads 1...n as union
		*/	export type Overloads<	T>
	=								T	extends{(...args:infer	A1	):infer	R1
		;										(...args:infer	A2	):infer	R2
		;										(...args:infer	A3	):infer	R3
		;										(...args:infer	A4	):infer	R4
		;										(...args:infer	A5	):infer	R5
		;										(...args:infer	A6	):infer	R6
		;										(...args:infer	A7	):infer	R7
		;										(...args:infer	A8	):infer	R8	}
	?	[	(									(...args:		A1	)=>		R1)
		|	(									(...args:		A2	)=>		R2)
		|	(									(...args:		A3	)=>		R3)
		|	(									(...args:		A4	)=>		R4)
		|	(									(...args:		A5	)=>		R5)
		|	(									(...args:		A6	)=>		R6)
		|	(									(...args:		A7	)=>		R7)
		|	(									(...args:		A8	)=>		R8)
		,	(									(...args:		A8	)=>		R8)
		,	(									(...args:		A7	)=>		R7)
		,	(									(...args:		A6	)=>		R6)
		,	(									(...args:		A5	)=>		R5)
		,	(									(...args:		A4	)=>		R4)
		,	(									(...args:		A3	)=>		R3)
		,	(									(...args:		A2	)=>		R2)
		,	(									(...args:		A1	)=>		R1)	]
	:								T	extends{(...args:infer	A1	):infer	R1
		;										(...args:infer	A2	):infer	R2
		;										(...args:infer	A3	):infer	R3
		;										(...args:infer	A4	):infer	R4
		;										(...args:infer	A5	):infer	R5
		;										(...args:infer	A6	):infer	R6
		;										(...args:infer	A7	):infer	R7	}
	?	[	(									(...args:		A1	)=>		R1)
		|	(									(...args:		A2	)=>		R2)
		|	(									(...args:		A3	)=>		R3)
		|	(									(...args:		A4	)=>		R4)
		|	(									(...args:		A5	)=>		R5)
		|	(									(...args:		A6	)=>		R6)
		|	(									(...args:		A7	)=>		R7)
		,	(									(...args:		A7	)=>		R7)
		,	(									(...args:		A6	)=>		R6)
		,	(									(...args:		A5	)=>		R5)
		,	(									(...args:		A4	)=>		R4)
		,	(									(...args:		A3	)=>		R3)
		,	(									(...args:		A2	)=>		R2)
		,	(									(...args:		A1	)=>		R1)	]
	:								T	extends{(...args:infer	A1	):infer	R1
		;										(...args:infer	A2	):infer	R2
		;										(...args:infer	A3	):infer	R3
		;										(...args:infer	A4	):infer	R4
		;										(...args:infer	A5	):infer	R5
		;										(...args:infer	A6	):infer	R6	}
	?	[	(									(...args:		A1	)=>		R1)
		|	(									(...args:		A2	)=>		R2)
		|	(									(...args:		A3	)=>		R3)
		|	(									(...args:		A4	)=>		R4)
		|	(									(...args:		A5	)=>		R5)
		|	(									(...args:		A6	)=>		R6)
		,	(									(...args:		A6	)=>		R6)
		,	(									(...args:		A5	)=>		R5)
		,	(									(...args:		A4	)=>		R4)
		,	(									(...args:		A3	)=>		R3)
		,	(									(...args:		A2	)=>		R2)
		,	(									(...args:		A1	)=>		R1)	]
	:								T	extends{(...args:infer	A1	):infer	R1
		;										(...args:infer	A2	):infer	R2
		;										(...args:infer	A3	):infer	R3
		;										(...args:infer	A4	):infer	R4
		;										(...args:infer	A5	):infer	R5	}
	?	[	(									(...args:		A1	)=>		R1)
		|	(									(...args:		A2	)=>		R2)
		|	(									(...args:		A3	)=>		R3)
		|	(									(...args:		A4	)=>		R4)
		|	(									(...args:		A5	)=>		R5)
		,	(									(...args:		A5	)=>		R5)
		,	(									(...args:		A4	)=>		R4)
		,	(									(...args:		A3	)=>		R3)
		,	(									(...args:		A2	)=>		R2)
		,	(									(...args:		A1	)=>		R1)	]
	:								T	extends{(...args:infer	A1	):infer	R1
		;										(...args:infer	A2	):infer	R2
		;										(...args:infer	A3	):infer	R3
		;										(...args:infer	A4	):infer	R4	}
	?	[	(									(...args:		A1	)=>		R1)
		|	(									(...args:		A2	)=>		R2)
		|	(									(...args:		A3	)=>		R3)
		|	(									(...args:		A4	)=>		R4)
		,	(									(...args:		A4	)=>		R4)
		,	(									(...args:		A3	)=>		R3)
		,	(									(...args:		A2	)=>		R2)
		,	(									(...args:		A1	)=>		R1)	]
	:								T	extends{(...args:infer	A1	):infer	R1
		;										(...args:infer	A2	):infer	R2
		;										(...args:infer	A3	):infer	R3	}
	?	[	(									(...args:		A1	)=>		R1)
		|	(									(...args:		A2	)=>		R2)
		|	(									(...args:		A3	)=>		R3)
		,	(									(...args:		A3	)=>		R3)
		,	(									(...args:		A2	)=>		R2)
		,	(									(...args:		A1	)=>		R1)	]
	:								T	extends{(...args:infer	A1	):infer	R1
		;										(...args:infer	A2	):infer	R2	}
	?	[	(									(...args:		A1	)=>		R1)
		|	(									(...args:		A2	)=>		R2)
		,	(									(...args:		A2	)=>		R2)
		,	(									(...args:		A1	)=>		R1)	]
	:								T	extends{(...args:infer	A1	):infer	R1	}
	?	[	(									(...args:		A1	)=>		R1)
		,	(									(...args:		A1	)=>		R1)	]
	:									never
	

	
	; /** Expands object types one level deep
		* Renamed and reformated from @see https://stackoverflow.com/questions/57683303/how-can-i-see-the-full-expanded-contract-of-a-typescript-type/57683652#57683652 @thanks https://stackoverflow.com/users/2887218/jcalz
		*/	export	type				   Expanded<T>
	=												T extends infer O
	? { [		K in keyof O]:						O[K]	}: never
	;		export	function			   expanded<T>(			v
		 :											T)
		{return<						   Expanded<T>><unknown>v}
	; /** Expands object types recursively
		* Renamed and reformated from @see https://stackoverflow.com/questions/57683303/how-can-i-see-the-full-expanded-contract-of-a-typescript-type/57683652#57683652 @thanks https://stackoverflow.com/users/2887218/jcalz
		*/	export	type		RecursivelyExpanded<T>
	=												T extends object
	?	(											T extends infer O
		? { [	K in keyof O]:	RecursivelyExpanded<O[K]>	}: never)
	:												T
	;		export	function	recursivelyExpanded<T>(			v
		 :											T)
		{return<				RecursivelyExpanded<T>><unknown>v}
	

	
	; /** renamed and formatted from @see https://stackoverflow.com/questions/53913496/how-to-type-constructor-as-well-as-constructor-argument-in-typescript/53943012#53943012 original
		*/	export type PickConstructor	<					T
			extends			new(...args:any[])=>InstanceType<	T>>
	=	{					new(...args:any[]):	InstanceType<	T>}
	;		export type ExtractConstructor	<					T
			extends			new(...args:any[])=>InstanceType<	T>>
	=					 InstanceType		<					T>
	

	
	; /** renamed and formatted from @see https://stackoverflow.com/questions/55344670/remove-item-from-inferred-paramters-tuple/55344772#55344772 original
		*/	export type		OmitFirst
		<						F			extends any[]>
	=	(	(...full	 :		F)=>void)	extends
		(	(	omit	 :infer	O
			,...rest	 :infer	R)=>void)
		?						R:never
	; /** @see				OmitFirst with magnitude of number
		*/	export type		OmitFirst2
		<						F			extends any[]>
	=	(	(...full	 :		F)=>void)	extends
		(	(	omit	 :infer	O
			,	omit2	 :infer	O2
			,...rest	 :infer	R)=>void)
		?						R:never
	; /** @see				OmitFirst with magnitude of number
		*/	export type		OmitFirst3
		<						F			extends any[]>
	=	(	(...full	 :		F)=>void)	extends
		(	(	omit	 :infer	O
			,	omit2	 :infer	O2
			,	omit3	 :infer	O3
			,...rest	 :infer	R)=>void)
		?						R:never
	; /** @note generic type params order follows TS builtin utility type Omit etc
		* @thanks https://stackoverflow.com/questions/55344670/remove-item-from-inferred-paramters-tuple/55344772#comment97414739_55344772
		*/	export type RestrictingOmitFirst
		<						F			extends any[]
		,						T>
	=	(	(...full	 :		F)=>void)	extends
		(	(	omit	 :		T
			,...rest	 :infer	R)=>void)
		?						R:never
	; /** @see			RestrictingOmitFirst with magnitude of number
		*/	export type	RestrictingOmitFirst2
		<						F			extends any[]
		,						O
		,						O2>
	=	(	(...full	 :		F)=>void)	extends
		(	(	omit	 :		O
			,	omit2	 :		O2
			,...rest	 :infer	R)=>void)
		?						R:never
	; /** @see			RestrictingOmitFirst with magnitude of number
		*/	export type	RestrictingOmitFirst3
		<						F			extends any[]
		,						O
		,						O2
		,						O3>
	=	(	(...full	 :		F)=>void)	extends
		(	(	omit	 :		O
			,	omit2	 :		O2
			,	omit3	 :		O3
			,...rest	 :infer	R)=>void)
		?						R:never
	

	
	;import{				expectingEqual}
	 from	  './expectancy/expecting'
	;namespace	test{
		;export const	OmitFirst2=()=>
			{				expectingEqual
				(	<	OmitFirst2<Parameters<(a1:'A1',a2:'A2',rest:'Rest')=>void>>	[0]>
				[																'Rest']			[0]
				,																'Rest')}
		;}
	

