////<reference	path	=	"../ext/ObjectConstructor/map/a.ts"	/>

	
	;import								   '../ext/ObjectConstructor/map/a'
	

	
	;		export			class				EnsurableMap					<K				,			V>
	 extends											 Map					<K				,			V>
		{public									ensure						(key:K
			,														factory:(key:K							)=>			V)
			{let	r=		this.	get										(key							)
			;if	(!	r)		this.	set										(key
				,	r=												factory	(key							))
			;return	r}
		;public					 async asyncEnsure							(key:K
			,														factory:(key:K							)=>Promise<	V>	) // can i put an async somewhere near the deferred value please? to get rid of that pesky explicitly literal Promise
			{let	r=		this.	get										(key							)
			;if	(!	r)		this.	set										(key
				,	r=			 await								factory	(key							))
			;return	r}}
	;		export			class			EnsurableWeakMap					<K extends {}	,			V>
	 extends										 WeakMap					<K				,			V>
		{public								ensure							(key:K
			,														factory:(key:K							)=>			V	)
			{let	r=		this.	get										(key							)
			;if	(!	r)		this.	set										(key
				,	r=												factory	(key							))
			;return	r																										}
		;public					 async asyncEnsure							(key:K
			,														factory:(key:K							)=>Promise<	V>	)
			{let	r=		this.	get										(key							)
			;if	(!	r)		this.	set										(key
				,	r=			 await								factory	(key							))
			;return	r																										}}
	

	
	;		export	type							 MapConstructorParameter
		< T	extends									 MapConstructor,											P1,P2
		>=T	extends										new			(initial:			Iterable<readonly	[	P1,P2]>
		|																				Array	<readonly	[	P1,P2]>		)=>any
	?																					Iterable<readonly	[	P1,P2]>
	|																					Array	<readonly	[	P1,P2]>		:never
	

	
	;		export	type			IndexingEnsurableMapArgument				<K				,			V>
	=												 MapConstructorParameter	<
		typeof										 Map						,K				,			V>
	;		export	type			IndexingEnsurableMapCache					<K				,	Indexed>
	=																			 K	extends	{}
	?										EnsurableWeakMap					<K	&		{}	,	Indexed>
	:										EnsurableMap						<K				,	Indexed>
	;/** @note AFAICS, getting `Indexed` inferred would be by `ReturnType<Indexing>`, which short circuits: “Type parameter defaults can only reference previously declared type parameters. ts(2744)”
		*/	export	type					EnsurableMapCacheIndexing			<K				,	Indexed>
	=																		(key:K		)=>			Indexed
	
	;		export	class			IndexingEnsurableMap						<K				,			V
		,																							Indexed
		=					string>
	 extends										 Map						<					Indexed,V>
		{ /** @see	constructor sets defaulting
			*/		public			indexing
		 :									EnsurableMapCacheIndexing			<K				,	Indexed>
		;			public								cache
		?:							IndexingEnsurableMapCache					<K				,	Indexed>
		;			constructor
			(											cache :boolean
			|						IndexingEnsurableMapCache					<K				,	Indexed> // is something equivalent using typeof or similar possible here?
			, /**		 sets `this.indexing` defaulting to this `||<Indexing><unknown>((k:any)=>''+k)` - note the "assertion"
				*/					indexing
			?:								EnsurableMapCacheIndexing			<K				,	Indexed>
			,			initials?:	IndexingEnsurableMapArgument				<					Indexed,V>)
			{if	(		initials
				)super(	initials)
			else super(			)
			;				this.	indexing
			=						indexing
			||	<							EnsurableMapCacheIndexing			<K				,	Indexed>>
				<unknown>(													(key:any	)=>
																		''	+key)
			;				this.						cache=(		()=>
			  {switch			(						cache)
			  {case false:return
			  ;case true :const
				 		initial
				= (	!	initials?[					undefined]
				  :	(	initials		instanceof	Array
					?	initials [0] // wtf - is the following evolved compared to prev??
					: (	initials[Symbol.	iterator]
						()				as	Iterator							<readonly[			Indexed,V]
						,														 readonly[			Indexed,V]>).next().value)
				  )					[0]
				;		  return( // new before paren (etc) is nogo @ts (tried before inner ass)
				  		initial			instanceof											{}.constructor
				  ?	<				IndexingEnsurableMapCache					<K	&		{}	,	Indexed>
					&						EnsurableWeakMap					<K	&		{}	,	Indexed> // & wtf here now??
					>new					EnsurableWeakMap					<K	&		{}	,	Indexed>()
				  :	<				IndexingEnsurableMapCache					<K	&		{}	,	Indexed>
					&						EnsurableMap						<K				,	Indexed>
					>new					EnsurableMap						<K				,	Indexed>())
			  ;default	 :return						cache}})	()
			;}
		;protected				   _indexingEnsure							(key:K											)
			{return			this.						cache
			?				this.						cache
			.								ensure							(key
				,			this.	indexing																)
			:				this.	indexing								(key							)				}
		;public								ensureIndexed					(key:					Indexed
			,														factory:(key:					Indexed	)=>			V	)
			{let	r=		this.			get								(key							)
			;if	(!	r)		this.			set								(key
				,	r=												factory	(key							))
			;return	r																										}
		;public					 async asyncEnsureIndexed					(key:					Indexed
			,														factory:(key:					Indexed	)=>Promise<	V>)
			{let	r=		this.			get								(key							)
			;if	(!	r)		this.			set								(key
				,	r=			 await								factory	(key							))
			;return	r																										}
		// if you're thinking the below could call the above it breaks on the incompatibility of the factories ;)
		;public								ensure							(key:K
			,														factory:(key:K							)=>			V	)
			{const																					indexed
			=				this.  _indexingEnsure							(key							)
			;let	r=		this.			get								(						indexed	)
			;if	(!	r)		this.			set								(						indexed
				,	r=												factory	(key							))
			;return	r																										}
		;public					 async asyncEnsure							(key:K
			,														factory:(key:K							)=>Promise<	V>	)
			{const																					indexed
			=				this.  _indexingEnsure							(key							)
			;let	r=		this.			get								(						indexed	)
			;if	(!	r)		this.			set								(						indexed
				,	r=			 await								factory	(key							))
			;return	r																										}
		;public						indexingGet								(key:K							)
			{const																					indexed
			=				this.  _indexingEnsure							(key							)
			;return			this.			get								(						indexed	)}
		;public						indexingSet								(key:K							,value:		V	)
			{const																					indexed
			=				this.  _indexingEnsure							(key							)
			;return			this.			set								(						indexed	,value)			}
		;public								getByIndexed					(key:					Indexed	)
			{return			this.			get								(key							)				}
		;public								setByIndexed					(key:					Indexed	,value:		V	)
			{return			this.			set								(key							,value)			}}
	
