
	
	;import{									OmitFirst3}
	 from					'../types'
	;import																				 {UnexpectedValueError}
	 from																			   './UnexpectedValueError'
	
	;		export function expectingTruthy		<	T>(	ret :				T
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
		{if	(											ret
			)									return	ret as NonNullable<	T>
		;else									throw								  new UnexpectedValueError
			(											ret
			,								 expectancyName,							  UnexpectedValueError.truthy
			,													...rest)}
	;		export function expectingEqual		<	T>(	ret :				T
		,												ref :				T
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
		{if	(											ret
			==											ref
			)									return	ret
		;else									throw								  new UnexpectedValueError
			(											ret
			,								 expectancyName
			,											ref,	...rest)}
/*	;		export function expectingEqualValue	<R,T>(	ret :			  R | T
		,												ref :
		typeof											ret	extends		R ? T : R/**/
	;		export function expectingEqualValue	<F			extends			T
		,											T>(	ret :				T
		,												ref :				F/**/
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
		{if	(											ret
			==											ref
			)									return	ret
		;else									throw								  new UnexpectedValueError
			(											ret
			,								 expectancyName
			,											ref,	...rest)}
// CLEANUP { after archive
/*	;		export function expectingTypeof			  (	ret : undefined
		,							 typeof_			    :'undefined'
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
	 :														  undefined
	;		export function expectingTypeof			  (	ret : boolean
		,							 typeof_			    :'boolean'
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
	 :														  boolean
	;		export function expectingTypeof			  (	ret : number
		,							 typeof_			    :'number'
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
	 :														  number
	;		export function expectingTypeof			  (	ret : bigint
		,							 typeof_			    :'bigint'
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
	 :														  bigint
	;		export function expectingTypeof			  (	ret : string
		,							 typeof_			    :'string'
		,							 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,												...rest
		 :								OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
	 :														  string
	;		export function expectingTypeof			  (	ret : symbol
		,							 typeof_			    :'symbol'
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
	 :														  symbol
	;		export function expectingTypeof		<	T extends ()=>any
		>											  (	ret :				T
		,							 typeof_			    :'function'
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
	 :																		T
	;		export function expectingTypeof			  (	ret : object
		,							 typeof_			    :'object'
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
	 :														  object
	;		export function expectingTypeof			  (	ret : any
		,							 typeof_			    : string
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)/**/
/*	;		export type Typeof					<	T>
	=												T extends undefined
	?														 'undefined'
	:												T extends boolean
	?														 'boolean'
	:												T extends number
	?														 'number'
	:												T extends bigint
	?														 'bigint'
	:												T extends string
	?														 'string'
	:												T extends symbol
	?														 'symbol'
	:												T extends ()=>any
	?														 'function'
	:												T extends object
	?														 'object'
	:														  never
	;		export function expectingTypeof		<	T>(	ret : 				T
		,							 typeof_			    : Typeof<		T>
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)/**/
//!CLEANUP } after archive
	;		export	type											Typeof
	=				typeof											TypeofToLeast
	;		export	const											TypeofToLeast
	=	{						undefined	:undefined
		,						boolean		:false
		,						number		:0
/*		,...'undefined'				!=typeof BigInt // since ts requires local declare var BigInt which, unless syncing every change to the whole lib es2020.bigint dts, this would yield a different type, i doubt this is a viable solution: //interface BigInt{}interface BigIntConstructor{(value?: any): bigint;readonly prototype: BigInt;}declare var BigInt:BigIntConstructor;
		&&	{					bigint		:BigInt()}	// lib.es2020.bigint is	not	a sound requirement 2020 /**/
		,						string		:''
		,						symbol		:Symbol()	// lib.es2015	"es6" is	a sound requirement 2020
		,						function	:()=><any>undefined
		,						object		:<{[key:string]:any}>{}}
	;		export function expectingTypeof	<		T extends keyof	Typeof
													 >(	ret :		Typeof[T]
		,							 typeof_			    :		T
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
	 :																Typeof[T]/**/
		{if	(						 typeof				ret
			==						 typeof_)	return	ret
		;else									throw								  new UnexpectedValueError
			(											ret
			,								 expectancyName,						  new UnexpectedValueError.SomethingTypeof
				(					 typeof_),					...rest)}
	;		export function expectingInstanceof	<	T		extends			C
		,																	C
													 >(	ret :				T
		,							 instanceof_		    :new()=>		C
		,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
		,														...rest
		 :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
		{if	(											ret
									 instanceof
									 instanceof_
			)									return	ret
		;else									throw								  new UnexpectedValueError
			(											ret
			,								 expectancyName,						  new UnexpectedValueError.SomethingInstanceof
				(					 instanceof_),			...rest)}
	
	;namespace	test
		{export	const	expectingTypeof_narrowingUnionOfBooleanAndString
		=()=>{	let						val :boolean|string ='true'
			;							val					= true
			;			expectingTypeof(val,'boolean')}}
	

