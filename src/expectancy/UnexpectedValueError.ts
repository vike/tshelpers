
	
	// SHOULD extend Symbol somehow
	; /** Extended by anonymous classes in UnexpectedValueError
		*/	export
			class												SomethingNamed
		{constructor
			(public														 name:string)
			{}
		;toString(){					   UnexpectedValueError
			.				localize	( 'UnexpectedValueError.SomethingNamed.'
				+			this										.name
				,											   "something"
				+' '+		this										.name)}}
	

	
	;		export
			class												SomethingTypeof
		{constructor
			(public														 type:string)
			{}
		;toString(){					   UnexpectedValueError
			.				localize	( 'UnexpectedValueError.SomethingTypeof.'
				+			this										.type
				,' '+		this										.type
				+' '+													"=="
				+														"typeof"
				+' '+										   "something")}}
	;		export
			class												SomethingInstanceof<		C>
		{constructor
			(public														 ctor:new()=>C)
			{}
		;toString(){					   UnexpectedValueError
			.				localize	( 'UnexpectedValueError.SomethingInstanceof.'
				+			this										.ctor
				,											   "something"
				+' '+													"instanceof"
				+' '+		this										.ctor)}}
	

	
	;import{		   dummyLocalize}		from		'vscode-localizability'
	

	
	;import										   {CustomError}
	 from									   'ts-custom-error'
	;export
	 class								   UnexpectedValueError
	 extends										CustomError
	 	{		static		localize
		=			   dummyLocalize
		
		;		static						truthy
		=	new	class						Truthy		extends	SomethingNamed{}
			(							   'truthy')
		;		static						null
		=	new	class						Null		extends	SomethingNamed{}
			(							   'null')
		;		static					   undefined
		=	new	class					   Undefined	extends	SomethingNamed{}
			(							  'undefined')
		;		static						 defined
		=	new	class						 Defined	extends	SomethingNamed{}
			(								'defined')
		
		;		static											SomethingTypeof
		=														SomethingTypeof
		;		static											SomethingInstanceof
		=														SomethingInstanceof
		
//		;	 public		readonly	name= 'UnexpectedValueError' // yielding same "TypeError: Cannot assign to read only property" as setting `this.name` in constructor body
		
		;		constructor
			(public						   unexpected					 :	any
			,public							 expectancyName				?:	string // because I'd find ‘a "name"’ more than ‘an expected value’ to be of value more in ‘pointing to where’ than in ‘how to solve’ the error occurred, I let all uses the two follow this precedence order. E.g.: localize('UnexpectedValueError.messageForNameExpectingSomethingParticular',"{0} for {1}, expecting {2}",undefined,"configurationKey",UnexpectedValueError.truthy) == `undefined for "configurationKey", expecting something Truthy`
			,public							 expecting					?:	any
			,													message	?:	string)
			{super(												message
				||			localize	( 'UnexpectedValueError.message'
					+						'Expecting'
					+	(undefined		==	 expectancyName	?'':'ForName')
					+	(undefined		==	 expecting		?	'Nothing'
						:									  'Something')+'Particular'
					,												'{0}'
					+	(undefined		==	 expectancyName	?''
						:			' '+	"for"			+' ' + '“{1}”')
					+	(undefined		==	 expecting?''
						:','	+	' '+	"expecting"		+' ' +	'{2}')
					,					   unexpected
					,						 expectancyName
					,						 expecting))}}
	

	
	;import{				localizeBy}		from		'vscode-localizability'
	;const					localize
	=						localizeBy(	   UnexpectedValueError)
	

