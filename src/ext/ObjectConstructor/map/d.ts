	 declare type							  ObjectConstructor_map
=	{	<														Source
		extends/*
			{	[														Key
				in										 keyof	Source
				]											:	Source[	Key]	}	|	*/	Iterable<
				[										 keyof	Source
				,												Source[
														 keyof	Source]]			>
		,														TargetValue
		,														Target
		extends{[														Key
				in										 keyof	Source
				]											:	TargetValue		}/*	&		Iterable<
				[										 keyof	Source
				,												TargetValue		]	>/**/			>
		(											from	:	Source
		,											into	:	Target				|		null
		,														mapper
		 :	<															Key
			extends										 keyof
									typeof			from>
			(											 val:
				(					typeof			from)[				Key]/*
														 keyof
									typeof			from]/**/
			,											 key:			Key
			,								 mappingFrom	:
									typeof			from
			,								 mappingInto	:							 NonNullable<
									typeof			into							>
			)												=>	TargetValue)
	:	{	[															Key
			in											 keyof	Source
			]												:	Target[	Key]	}
	;	<														Source
		extends/*
			{	[														Key
				in										 keyof	Source
				]											:	Source[	Key]	}	|	*/	Iterable<
				[										 keyof	Source
				,												Source[
														 keyof	Source]]			>
		,														TargetValue
		,														Target
		extends{[														Key
				in										 keyof	Source
				]											:	TargetValue		}/*	&		Iterable<
				[										 keyof	Source
				,												TargetValue		]	>/**/			>
		(											from	:	Source
		,														mapper
		 :	<															Key
			extends										 keyof
									typeof			from>
			(											 val:
				(					typeof			from)[				Key]/*
														 keyof
									typeof			from]/**/
			,											 key:			Key
			,								 mappingFrom	:
									typeof			from
			,								 mappingInto	:							 NonNullable<
																Target				>
			)												=>	TargetValue)
	:	{	[															Key
			in											 keyof	Source
			]												:	Target[	Key]	}
	;	<														Source
		extends{[														Key
				in										 keyof	Source
				]											:	Source[	Key]	}
		,														TargetValue
		,														Target
		extends{[														Key
				in										 keyof	Source
				]											:	TargetValue		}>
		(											from	:	Source
		,											into	:	Target				|		null
		,														mapper
		 :	<															Key
			extends										 keyof
									typeof			from>
			(											 val:
				(					typeof			from)[				Key]/*
														 keyof
									typeof			from]/**/
			,											 key:			Key
			,								 mappingFrom	:
									typeof			from
			,								 mappingInto	:							 NonNullable<
									typeof			into							>
			)												=>	TargetValue)
	:	{	[															Key
			in											 keyof	Source
			]												:	Target[	Key]	}
	;	<														Source
		extends{[														Key
				in										 keyof	Source
				]											:	Source[	Key]	}
		,														TargetValue
		,														Target
		extends{[														Key
				in										 keyof	Source
				]											:	TargetValue		}>
		(											from	:	Source
		,														mapper
		 :	<															Key
			extends										 keyof
									typeof			from>
			(											 val:
				(					typeof			from)[				Key]/*
														 keyof
									typeof			from]/**/
			,											 key:			Key
			,								 mappingFrom	:
									typeof			from
			,								 mappingInto	:	Target
			)												=>	TargetValue)
	:	{	[															Key
			in											 keyof	Source
			]:													Target[	Key]	}
	}