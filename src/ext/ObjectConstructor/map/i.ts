	;export default function									map
		<														Source
		extends{[														Key
				in										 keyof	Source
				]											:	Source[	Key]	}	|		Iterable<
				[										 keyof	Source
				,												Source[
														 keyof	Source]]			>
		,														Target
		extends{[														Key
				in										 keyof	Source
				]											:	TargetValue		}
		,														TargetValue							>
		(											from	:	Source
		,											into	:	Target				|		null
		,														mapper
		 :	<															Key
			extends										 keyof
									typeof			from>
			(											 val:
				(					typeof			from)[				Key]
			,											 key:			Key
			,								 mappingFrom	:
									typeof			from
			,								 mappingInto	:							 NonNullable<
									typeof			into>
			)												=>	TargetValue)
	:															Target
		{if	(!													mapper
			)	[												mapper
				,									into	]
			=	[									into	as
									typeof						mapper
				,									null	]
		;const							   effectiveInto
		=	(										into||{
			...										from   })as							 NonNullable<
									typeof			into>
		;if	(					Symbol.iterator in	from) // COULD import {isIterable} from 'iteration-typeguards'
			{const						   effectiveFrom
			=										from	 as								Iterable<
					[									 keyof	Source
					,											Source[
														 keyof	Source]]>
			;for(let[									 key
					,									 val]
				of						   effectiveFrom
				)						   effectiveInto[key]
				=												mapper
				(										 val as
					(				typeof			from)[
														 keyof
									typeof			from]
				,										 key as
														 keyof
									typeof			from
				,									from
				,									into	 as							 NonNullable<
									typeof			into>)	 as
									typeof effectiveInto[
									typeof				 key]}
		else{const						   effectiveFrom
			=										from	 as	Source
			;for(let									 key
				in						   effectiveFrom
				)						   effectiveInto[key]
				=												mapper
				(						   effectiveFrom[key]as
					(				typeof			from)[
														 keyof
									typeof			from]
				,										 key as
														 keyof
									typeof			from
				,						   effectiveFrom	 as
									typeof			from
				,									into	 as							 NonNullable<
									typeof			into>)	 as
									typeof effectiveInto[
									typeof				 key]}
		;return							   effectiveInto}
	

	
//	;import							  {UnexpectedValueError}
//	 from		  '../../../expectancy/UnexpectedValueError'
	


/* https://www.typescriptlang.org/play?target=99&jsx=0#code/CYUwxgNghgTiAEAzArgOzAFwJYHtXwHcALEVAVQGctUBzeeAClhuQFtSMpsA3ECATwBc9EaPoMMRLBWHxKUGiAA08AHTqADrCjsMIGDKip+ASngBeAHzxuOLMBODb9gFChIsBB4oUxf-wGBQX7yivAA3i6iyBQKCMEJiUGCktJRIkRQvkl+DGB4wFjYeH6CaADWqDgEqGb0KVIU6fRZFPoYFAByeJ3IEBBBDNywWFAARhDxYmWoldW1Ig3SAL4uWIiMAOQYMMggm2aSMNXwm90YSDgw8LuoqNQ0wgAi4NAwXFi88HgCmy4A9P94AAJfQIMYIGgQPaXa5QTDIKADAAGWFYGiuF3CxFIlAeKlCIGWSGOrFOqn+MX2yJcKHQxXwBCKRAAkujMQwtJJBOUQPwcBsMPwNCABfAqKw+lwQMAAPpojEwDomcJwDDIGD4CVSvRyhWYigAbS5RAAuqtJhdtdBdfL2UrfOYIs1yZS2pthOFCCRyFRaATYmFVqsXEzJGzFRgGJsKVSDqpQz68bQGC66ZhcPgsgxEKSZnMaipqBgcPmqoW1OoQBgwNX9FBBEZTC7wqkKKoYnFVJkKAwAMr8VhjHAQVRFesl67URi5nBkrLwJsmEyqVrtLo9PoQBjFnAmVYmIA
declare function whenUsing  (argumentatively:           (this:  Usage, ...parameters:any) => void):void
declare class                                                   Usage {
    usage                                               :this
    has                     (condition      :unknown)   :this
    assertsNonNull          (variable       :unknown)   :this}
if ('true') throw 'Not for running: Declarative only'
// Here be glue for actual `import {whenUsing, Usage} from './use'`
function withImport(path:keyof typeof simulated_imports){return simulated_imports[path]}
let simulated_imports = {
    './use': { whenUsing, Usage }
}

withImport('./use').
whenUsing(
    function as(from:unknown, into:unknown, ...etcetera:any)
    {this.usage.has(Symbol.iterator in (from as any)).assertsNonNull(into)}
)
*/

/* https://www.typescriptlang.org/play/?target=2&module=1#code/JYOwLgpgTgZghgYwgAgJIB4CaA+ZBvZAbQHJViBdALmU2QF9kB6R5KCOAGw4E9kPgARgDoIAZwBMABgCMAVmQADVJChwBHCOgAqmYBA4ATbAoBQMAK4gEYYAHsQySKLBMWbTj2RwHCgPICAKwhrIQBbOAAHBWRgUIiNUIhwOBt7E2QM5HR0zNyAZWQIAA9IEANRfCIAaRiHAGsIblsYZDyqVsIq8noAGhzcjIAxQpKk8srO2uQGppa26kHO7rp+5GxVgApV3NFqAuLS8u2BjIwCSdBpxubW9ryl+lwAfmOTs+qpmZv5jq7H5EorwGeT6JwyMD2I0OoiBuXeF3q1zmdwedGesMy8Jqly+yIWqNwgLBAw2IHMXGQAB9kIMAJSrel4VYWKypBxwUSiaBsjYwAD61AAcvZBeSOGoNOgwNwIhAbjBsLTKsAWhsAIT8pVgAAWUFsAHdkMQOVyoGzkPBgBoDMR6K5WOwuLw4Io2GBzFAQNE4DAVF5FDBbFANrTTKsVcgNqRbZdREqCMw0MRQsgAI7mYCQZCiD0oF0Ac3McCgBmQNwAbpxzCh7I4oOYdbxIFwKloCpWONWYqIQMQXGSuD1kAIG8gDLYxI5tcAKgoVdEQLYXM2OMg214QNwdaB8y8AJAHg+J1AVHXQPOR0SxeIQJUCCBgP36uBN2zIBDa4J1S9gevWD2cEq14JEkYApHYDg3KAKjwEgADc3ZTigCD2AYmYQZwijSrKNyiN6DgzjmEAvMSKEgM4Fp8sgAC8FpAia3IQbyVEcsgwogKKXASpo2Fyi0CpKomCgctECBwOYXJlhCa4FOOYi9v2xZ6oaZFoWymG8aIJGZAw+iSUypH2BR-I0RaVLUngDCsexnHiuoPEynxFq4ImqnofYmECKoVjaqwlg2IkY4QPA5JgPRnKMfYzFeBUNlitxUqOfKir2ggLRqLY5YQKsKwrE4YAbJZQ4DhwgksLYdQviY+WFdG1DEHkxB0EOeB1UagxNWVZaVdw1ViAVBBtQ1trNcgJX0omACiAAaAAKk0AMJaJNAAi1AnAAfltG3IAAglAhaJOAUmOI5Y1iohi4uCawD5iA3GOG+WKfEitz4n8aImImu1LQAqrtAAy62ZL9Vjifm2ouMaEVmhBFpwFaEA2l9LALeJknNNQ65yT2fZjUpBrvqh7n3aumkvIms1CLsyCWDmbAxMmtNXiA+bIAARPdUDKezRPc8EYBAA
interface I<Y> { ['I']: Y } // really lib.es2015 `Iterable<TYield>`
function test // really an `Object.map` implementation
    <
        S extends { [K in keyof S]: S[K] },
        F extends { [K in keyof S]: F[K] }
    >
    (
        s: S extends
            I<{ [K in keyof S]: S[K] }> ?
            I<{ [K in keyof S]: S[K] }> :
            S,
        f: S extends
            I<{ [K in keyof S]: S[K] }> ?
            I<{ [K in keyof S]: F[K] }> :
            (null | F)
    )
{
    function assertion(f_: NonNullable<typeof f>) { if (!f_) throw 'assertion failed' } // really a `return` after a `for()`

    if ('I' in s) { // I'm quite sure a guard of value on truthy tells TS value isn't null, but does this `if` not tell TS anything?
					// Is there a (simple) better way to check (structural) implementation of interface; is the conditional `typeof s` an issue?
        const f_ = f
        assertion(f_ as NonNullable<typeof f>) // `as` cause of: TS doesn't narrow conditional types?
    } else {
        const f_ = f || {} as NonNullable<typeof f> // conditional branch runtime default
        assertion(f_ as NonNullable<typeof f>) // cf above
    }
}
test({}, null) // okay
test({'I': 'S'}, {'I': 'F'}) // okay
test({ 'I': 'S' }, null)
// EXPECTED:       ~~~~ Argument of type null is not assignable to I<{ [K in keyof S]: F[K] }>
// ACTUAL:     Uncaught 'assertion failed'
// Cause of: TS doesn't narrow conditional types?
// P.s: unsure i'm using "narrow" correct
*/