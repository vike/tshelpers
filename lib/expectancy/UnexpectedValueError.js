"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// SHOULD extend Symbol somehow
; /** Extended by anonymous classes in UnexpectedValueError
    */
class SomethingNamed {
    constructor(name) {
        this.name = name;
    }
    ;
    toString() {
        UnexpectedValueError
            .localize('UnexpectedValueError.SomethingNamed.'
            + this.name, "something"
            + ' ' + this.name);
    }
}
exports.SomethingNamed = SomethingNamed;
;
class SomethingTypeof {
    constructor(type) {
        this.type = type;
    }
    ;
    toString() {
        UnexpectedValueError
            .localize('UnexpectedValueError.SomethingTypeof.'
            + this.type, ' ' + this.type
            + ' ' + "=="
            + "typeof"
            + ' ' + "something");
    }
}
exports.SomethingTypeof = SomethingTypeof;
;
class SomethingInstanceof {
    constructor(ctor) {
        this.ctor = ctor;
    }
    ;
    toString() {
        UnexpectedValueError
            .localize('UnexpectedValueError.SomethingInstanceof.'
            + this.ctor, "something"
            + ' ' + "instanceof"
            + ' ' + this.ctor);
    }
}
exports.SomethingInstanceof = SomethingInstanceof;
;
const vscode_localizability_1 = require("vscode-localizability");
const ts_custom_error_1 = require("ts-custom-error");
class UnexpectedValueError extends ts_custom_error_1.CustomError {
    constructor(unexpected, expectancyName // because I'd find ‘a "name"’ more than ‘an expected value’ to be of value more in ‘pointing to where’ than in ‘how to solve’ the error occurred, I let all uses the two follow this precedence order. E.g.: localize('UnexpectedValueError.messageForNameExpectingSomethingParticular',"{0} for {1}, expecting {2}",undefined,"configurationKey",UnexpectedValueError.truthy) == `undefined for "configurationKey", expecting something Truthy`
    , expecting, message) {
        super(message
            || localize('UnexpectedValueError.message'
                + 'Expecting'
                + (undefined == expectancyName ? '' : 'ForName')
                + (undefined == expecting ? 'Nothing'
                    : 'Something') + 'Particular', '{0}'
                + (undefined == expectancyName ? ''
                    : ' ' + "for" + ' ' + '“{1}”')
                + (undefined == expecting ? ''
                    : ',' + ' ' + "expecting" + ' ' + '{2}'), unexpected, expectancyName, expecting));
        this.unexpected = unexpected;
        this.expectancyName = expectancyName;
        this.expecting = expecting;
    }
}
exports.UnexpectedValueError = UnexpectedValueError;
UnexpectedValueError.localize = vscode_localizability_1.dummyLocalize;
UnexpectedValueError.truthy = new class Truthy extends SomethingNamed {
}('truthy');
UnexpectedValueError.null = new class Null extends SomethingNamed {
}('null');
UnexpectedValueError.undefined = new class Undefined extends SomethingNamed {
}('undefined');
UnexpectedValueError.defined = new class Defined extends SomethingNamed {
}('defined');
UnexpectedValueError.SomethingTypeof = SomethingTypeof;
UnexpectedValueError.SomethingInstanceof = SomethingInstanceof;
;
const vscode_localizability_2 = require("vscode-localizability");
const localize = vscode_localizability_2.localizeBy(UnexpectedValueError);
//# sourceMappingURL=UnexpectedValueError.js.map