import { OmitFirst3 } from '../types';
import { UnexpectedValueError } from './UnexpectedValueError';
export declare function expectingTruthy<T>(ret: T, expectancyName?: ConstructorParameters<typeof UnexpectedValueError>[1], ...rest: OmitFirst3<ConstructorParameters<typeof UnexpectedValueError>>): NonNullable<T>;
export declare function expectingEqual<T>(ret: T, ref: T, expectancyName?: ConstructorParameters<typeof UnexpectedValueError>[1], ...rest: OmitFirst3<ConstructorParameters<typeof UnexpectedValueError>>): T;
export declare function expectingEqualValue<F extends T, T>(ret: T, ref: F, expectancyName?: ConstructorParameters<typeof UnexpectedValueError>[1], ...rest: OmitFirst3<ConstructorParameters<typeof UnexpectedValueError>>): T;
export declare type Typeof = typeof TypeofToLeast;
export declare const TypeofToLeast: {
    undefined: undefined;
    boolean: boolean;
    number: number;
    string: string;
    symbol: symbol;
    function: () => any;
    object: {
        [key: string]: any;
    };
};
export declare function expectingTypeof<T extends keyof Typeof>(ret: Typeof[T], typeof_: T, expectancyName?: ConstructorParameters<typeof UnexpectedValueError>[1], ...rest: OmitFirst3<ConstructorParameters<typeof UnexpectedValueError>>): Typeof[T];
export declare function expectingInstanceof<T extends C, C>(ret: T, instanceof_: new () => C, expectancyName?: ConstructorParameters<typeof UnexpectedValueError>[1], ...rest: OmitFirst3<ConstructorParameters<typeof UnexpectedValueError>>): T;
//# sourceMappingURL=expecting.d.ts.map