"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
;
const UnexpectedValueError_1 = require("./UnexpectedValueError");
function expectingTruthy(ret, expectancyName, ...rest) {
    if (ret)
        return ret;
    else
        throw new UnexpectedValueError_1.UnexpectedValueError(ret, expectancyName, UnexpectedValueError_1.UnexpectedValueError.truthy, ...rest);
}
exports.expectingTruthy = expectingTruthy;
;
function expectingEqual(ret, ref, expectancyName, ...rest) {
    if (ret
        == ref)
        return ret;
    else
        throw new UnexpectedValueError_1.UnexpectedValueError(ret, expectancyName, ref, ...rest);
}
exports.expectingEqual = expectingEqual;
/*	;		export function expectingEqualValue	<R,T>(	ret :			  R | T
        ,												ref :
        typeof											ret	extends		R ? T : R/**/
;
function expectingEqualValue(ret, ref /**/, expectancyName, ...rest) {
    if (ret
        == ref)
        return ret;
    else
        throw new UnexpectedValueError_1.UnexpectedValueError(ret, expectancyName, ref, ...rest);
}
exports.expectingEqualValue = expectingEqualValue;
// CLEANUP { after archive
/*	;		export function expectingTypeof			  (	ret : undefined
        ,							 typeof_			    :'undefined'
        ,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
        ,														...rest
         :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
     :														  undefined
    ;		export function expectingTypeof			  (	ret : boolean
        ,							 typeof_			    :'boolean'
        ,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
        ,														...rest
         :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
     :														  boolean
    ;		export function expectingTypeof			  (	ret : number
        ,							 typeof_			    :'number'
        ,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
        ,														...rest
         :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
     :														  number
    ;		export function expectingTypeof			  (	ret : bigint
        ,							 typeof_			    :'bigint'
        ,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
        ,														...rest
         :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
     :														  bigint
    ;		export function expectingTypeof			  (	ret : string
        ,							 typeof_			    :'string'
        ,							 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
        ,												...rest
         :								OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
     :														  string
    ;		export function expectingTypeof			  (	ret : symbol
        ,							 typeof_			    :'symbol'
        ,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
        ,														...rest
         :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
     :														  symbol
    ;		export function expectingTypeof		<	T extends ()=>any
        >											  (	ret :				T
        ,							 typeof_			    :'function'
        ,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
        ,														...rest
         :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
     :																		T
    ;		export function expectingTypeof			  (	ret : object
        ,							 typeof_			    :'object'
        ,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
        ,														...rest
         :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)
     :														  object
    ;		export function expectingTypeof			  (	ret : any
        ,							 typeof_			    : string
        ,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
        ,														...rest
         :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)/**/
/*	;		export type Typeof					<	T>
    =												T extends undefined
    ?														 'undefined'
    :												T extends boolean
    ?														 'boolean'
    :												T extends number
    ?														 'number'
    :												T extends bigint
    ?														 'bigint'
    :												T extends string
    ?														 'string'
    :												T extends symbol
    ?														 'symbol'
    :												T extends ()=>any
    ?														 'function'
    :												T extends object
    ?														 'object'
    :														  never
    ;		export function expectingTypeof		<	T>(	ret : 				T
        ,							 typeof_			    : Typeof<		T>
        ,									 expectancyName?:ConstructorParameters<typeof UnexpectedValueError>[1]
        ,														...rest
         :										OmitFirst3	<ConstructorParameters<typeof UnexpectedValueError>>)/**/
//!CLEANUP } after archive
;
exports.TypeofToLeast = { undefined: undefined,
    boolean: false,
    number: 0
    /*		,...'undefined'				!=typeof BigInt // since ts requires local declare var BigInt which, unless syncing every change to the whole lib es2020.bigint dts, this would yield a different type, i doubt this is a viable solution: //interface BigInt{}interface BigIntConstructor{(value?: any): bigint;readonly prototype: BigInt;}declare var BigInt:BigIntConstructor;
            &&	{					bigint		:BigInt()}	// lib.es2020.bigint is	not	a sound requirement 2020 /**/
    ,
    string: '',
    symbol: Symbol() // lib.es2015	"es6" is	a sound requirement 2020
    ,
    function: () => undefined,
    object: {} };
function expectingTypeof(ret, typeof_, expectancyName, ...rest) {
    if (typeof ret
        == typeof_)
        return ret;
    else
        throw new UnexpectedValueError_1.UnexpectedValueError(ret, expectancyName, new UnexpectedValueError_1.UnexpectedValueError.SomethingTypeof(typeof_), ...rest);
}
exports.expectingTypeof = expectingTypeof;
;
function expectingInstanceof(ret, instanceof_, expectancyName, ...rest) {
    if (ret
        instanceof
            instanceof_)
        return ret;
    else
        throw new UnexpectedValueError_1.UnexpectedValueError(ret, expectancyName, new UnexpectedValueError_1.UnexpectedValueError.SomethingInstanceof(instanceof_), ...rest);
}
exports.expectingInstanceof = expectingInstanceof;
;
var test;
(function (test) {
    test.expectingTypeof_narrowingUnionOfBooleanAndString = () => {
        let val = 'true';
        val = true;
        expectingTypeof(val, 'boolean');
    };
})(test || (test = {}));
//# sourceMappingURL=expecting.js.map