export declare class SomethingNamed {
    name: string;
    constructor(name: string);
    toString(): void;
}
export declare class SomethingTypeof {
    type: string;
    constructor(type: string);
    toString(): void;
}
export declare class SomethingInstanceof<C> {
    ctor: new () => C;
    constructor(ctor: new () => C);
    toString(): void;
}
import { dummyLocalize } from 'vscode-localizability';
import { CustomError } from 'ts-custom-error';
export declare class UnexpectedValueError extends CustomError {
    unexpected: any;
    expectancyName?: string | undefined;
    expecting?: any;
    static localize: typeof dummyLocalize;
    static truthy: {
        name: string;
        toString(): void;
    };
    static null: {
        name: string;
        toString(): void;
    };
    static undefined: {
        name: string;
        toString(): void;
    };
    static defined: {
        name: string;
        toString(): void;
    };
    static SomethingTypeof: typeof SomethingTypeof;
    static SomethingInstanceof: typeof SomethingInstanceof;
    constructor(unexpected: any, expectancyName?: string | undefined, expecting?: any, message?: string);
}
//# sourceMappingURL=UnexpectedValueError.d.ts.map