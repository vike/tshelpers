export declare type Overloads<T> = T extends {
    (...args: infer A1): infer R1;
    (...args: infer A2): infer R2;
    (...args: infer A3): infer R3;
    (...args: infer A4): infer R4;
    (...args: infer A5): infer R5;
    (...args: infer A6): infer R6;
    (...args: infer A7): infer R7;
    (...args: infer A8): infer R8;
} ? [((...args: A1) => R1) | ((...args: A2) => R2) | ((...args: A3) => R3) | ((...args: A4) => R4) | ((...args: A5) => R5) | ((...args: A6) => R6) | ((...args: A7) => R7) | ((...args: A8) => R8), ((...args: A8) => R8), ((...args: A7) => R7), ((...args: A6) => R6), ((...args: A5) => R5), ((...args: A4) => R4), ((...args: A3) => R3), ((...args: A2) => R2), ((...args: A1) => R1)] : T extends {
    (...args: infer A1): infer R1;
    (...args: infer A2): infer R2;
    (...args: infer A3): infer R3;
    (...args: infer A4): infer R4;
    (...args: infer A5): infer R5;
    (...args: infer A6): infer R6;
    (...args: infer A7): infer R7;
} ? [((...args: A1) => R1) | ((...args: A2) => R2) | ((...args: A3) => R3) | ((...args: A4) => R4) | ((...args: A5) => R5) | ((...args: A6) => R6) | ((...args: A7) => R7), ((...args: A7) => R7), ((...args: A6) => R6), ((...args: A5) => R5), ((...args: A4) => R4), ((...args: A3) => R3), ((...args: A2) => R2), ((...args: A1) => R1)] : T extends {
    (...args: infer A1): infer R1;
    (...args: infer A2): infer R2;
    (...args: infer A3): infer R3;
    (...args: infer A4): infer R4;
    (...args: infer A5): infer R5;
    (...args: infer A6): infer R6;
} ? [((...args: A1) => R1) | ((...args: A2) => R2) | ((...args: A3) => R3) | ((...args: A4) => R4) | ((...args: A5) => R5) | ((...args: A6) => R6), ((...args: A6) => R6), ((...args: A5) => R5), ((...args: A4) => R4), ((...args: A3) => R3), ((...args: A2) => R2), ((...args: A1) => R1)] : T extends {
    (...args: infer A1): infer R1;
    (...args: infer A2): infer R2;
    (...args: infer A3): infer R3;
    (...args: infer A4): infer R4;
    (...args: infer A5): infer R5;
} ? [((...args: A1) => R1) | ((...args: A2) => R2) | ((...args: A3) => R3) | ((...args: A4) => R4) | ((...args: A5) => R5), ((...args: A5) => R5), ((...args: A4) => R4), ((...args: A3) => R3), ((...args: A2) => R2), ((...args: A1) => R1)] : T extends {
    (...args: infer A1): infer R1;
    (...args: infer A2): infer R2;
    (...args: infer A3): infer R3;
    (...args: infer A4): infer R4;
} ? [((...args: A1) => R1) | ((...args: A2) => R2) | ((...args: A3) => R3) | ((...args: A4) => R4), ((...args: A4) => R4), ((...args: A3) => R3), ((...args: A2) => R2), ((...args: A1) => R1)] : T extends {
    (...args: infer A1): infer R1;
    (...args: infer A2): infer R2;
    (...args: infer A3): infer R3;
} ? [((...args: A1) => R1) | ((...args: A2) => R2) | ((...args: A3) => R3), ((...args: A3) => R3), ((...args: A2) => R2), ((...args: A1) => R1)] : T extends {
    (...args: infer A1): infer R1;
    (...args: infer A2): infer R2;
} ? [((...args: A1) => R1) | ((...args: A2) => R2), ((...args: A2) => R2), ((...args: A1) => R1)] : T extends {
    (...args: infer A1): infer R1;
} ? [((...args: A1) => R1), ((...args: A1) => R1)] : never; /** Expands object types one level deep
    * Renamed and reformated from @see https://stackoverflow.com/questions/57683303/how-can-i-see-the-full-expanded-contract-of-a-typescript-type/57683652#57683652 @thanks https://stackoverflow.com/users/2887218/jcalz
    */
export declare type Expanded<T> = T extends infer O ? {
    [K in keyof O]: O[K];
} : never;
export declare function expanded<T>(v: T): Expanded<T>;
export declare type RecursivelyExpanded<T> = T extends object ? (T extends infer O ? {
    [K in keyof O]: RecursivelyExpanded<O[K]>;
} : never) : T;
export declare function recursivelyExpanded<T>(v: T): RecursivelyExpanded<T>;
export declare type PickConstructor<T extends new (...args: any[]) => InstanceType<T>> = {
    new (...args: any[]): InstanceType<T>;
};
export declare type ExtractConstructor<T extends new (...args: any[]) => InstanceType<T>> = InstanceType<T>; /** renamed and formatted from @see https://stackoverflow.com/questions/55344670/remove-item-from-inferred-paramters-tuple/55344772#55344772 original
    */
export declare type OmitFirst<F extends any[]> = ((...full: F) => void) extends ((omit: infer O, ...rest: infer R) => void) ? R : never; /** @see				OmitFirst with magnitude of number
    */
export declare type OmitFirst2<F extends any[]> = ((...full: F) => void) extends ((omit: infer O, omit2: infer O2, ...rest: infer R) => void) ? R : never; /** @see				OmitFirst with magnitude of number
    */
export declare type OmitFirst3<F extends any[]> = ((...full: F) => void) extends ((omit: infer O, omit2: infer O2, omit3: infer O3, ...rest: infer R) => void) ? R : never; /** @note generic type params order follows TS builtin utility type Omit etc
    * @thanks https://stackoverflow.com/questions/55344670/remove-item-from-inferred-paramters-tuple/55344772#comment97414739_55344772
    */
export declare type RestrictingOmitFirst<F extends any[], T> = ((...full: F) => void) extends ((omit: T, ...rest: infer R) => void) ? R : never; /** @see			RestrictingOmitFirst with magnitude of number
    */
export declare type RestrictingOmitFirst2<F extends any[], O, O2> = ((...full: F) => void) extends ((omit: O, omit2: O2, ...rest: infer R) => void) ? R : never; /** @see			RestrictingOmitFirst with magnitude of number
    */
export declare type RestrictingOmitFirst3<F extends any[], O, O2, O3> = ((...full: F) => void) extends ((omit: O, omit2: O2, omit3: O3, ...rest: infer R) => void) ? R : never;
//# sourceMappingURL=types.d.ts.map