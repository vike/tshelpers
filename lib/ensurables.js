"use strict";
////<reference	path	=	"../ext/ObjectConstructor/map/a.ts"	/>
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
;
require("../ext/ObjectConstructor/map/a");
class EnsurableMap extends Map {
    ensure(key, factory) {
        let r = this.get(key);
        if (!r)
            this.set(key, r = factory(key));
        return r;
    }
    ;
    asyncEnsure(key, factory) {
        return __awaiter(this, void 0, void 0, function* () {
            let r = this.get(key);
            if (!r)
                this.set(key, r = yield factory(key));
            return r;
        });
    }
}
exports.EnsurableMap = EnsurableMap;
;
class EnsurableWeakMap extends WeakMap {
    ensure(key, factory) {
        let r = this.get(key);
        if (!r)
            this.set(key, r = factory(key));
        return r;
    }
    ;
    asyncEnsure(key, factory) {
        return __awaiter(this, void 0, void 0, function* () {
            let r = this.get(key);
            if (!r)
                this.set(key, r = yield factory(key));
            return r;
        });
    }
}
exports.EnsurableWeakMap = EnsurableWeakMap;
;
class IndexingEnsurableMap extends Map {
    constructor(cache // is something equivalent using typeof or similar possible here?
    , /**		 sets `this.indexing` defaulting to this `||<Indexing><unknown>((k:any)=>''+k)` - note the "assertion"
        */ indexing, initials) {
        if (initials)
            super(initials);
        else
            super();
        this.indexing
            = indexing
                || ((key) => '' + key);
        this.cache = (() => {
            switch (cache) {
                case false: return;
                case true:
                    const initial = (!initials ? [undefined]
                        : (initials instanceof Array
                            ? initials[0] // wtf - is the following evolved compared to prev??
                            : initials[Symbol.iterator]().next().value))[0];
                    return ( // new before paren (etc) is nogo @ts (tried before inner ass)
                    initial instanceof {}.constructor
                        ? new EnsurableWeakMap()
                        : new EnsurableMap());
                default: return cache;
            }
        })();
    }
    ;
    _indexingEnsure(key) {
        return this.cache
            ? this.cache
                .ensure(key, this.indexing)
            : this.indexing(key);
    }
    ;
    ensureIndexed(key, factory) {
        let r = this.get(key);
        if (!r)
            this.set(key, r = factory(key));
        return r;
    }
    ;
    asyncEnsureIndexed(key, factory) {
        return __awaiter(this, void 0, void 0, function* () {
            let r = this.get(key);
            if (!r)
                this.set(key, r = yield factory(key));
            return r;
        });
    }
    // if you're thinking the below could call the above it breaks on the incompatibility of the factories ;)
    ;
    ensure(key, factory) {
        const indexed = this._indexingEnsure(key);
        let r = this.get(indexed);
        if (!r)
            this.set(indexed, r = factory(key));
        return r;
    }
    ;
    asyncEnsure(key, factory) {
        return __awaiter(this, void 0, void 0, function* () {
            const indexed = this._indexingEnsure(key);
            let r = this.get(indexed);
            if (!r)
                this.set(indexed, r = yield factory(key));
            return r;
        });
    }
    ;
    indexingGet(key) {
        const indexed = this._indexingEnsure(key);
        return this.get(indexed);
    }
    ;
    indexingSet(key, value) {
        const indexed = this._indexingEnsure(key);
        return this.set(indexed, value);
    }
    ;
    getByIndexed(key) { return this.get(key); }
    ;
    setByIndexed(key, value) { return this.set(key, value); }
}
exports.IndexingEnsurableMap = IndexingEnsurableMap;
//# sourceMappingURL=ensurables.js.map