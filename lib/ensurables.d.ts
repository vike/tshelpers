import '../ext/ObjectConstructor/map/a';
export declare class EnsurableMap<K, V> extends Map<K, V> {
    ensure(key: K, factory: (key: K) => V): V;
    asyncEnsure(key: K, factory: (key: K) => Promise<V>): Promise<V>;
}
export declare class EnsurableWeakMap<K extends {}, V> extends WeakMap<K, V> {
    ensure(key: K, factory: (key: K) => V): V;
    asyncEnsure(key: K, factory: (key: K) => Promise<V>): Promise<V>;
}
export declare type MapConstructorParameter<T extends MapConstructor, P1, P2> = T extends new (initial: Iterable<readonly [P1, P2]> | Array<readonly [P1, P2]>) => any ? Iterable<readonly [P1, P2]> | Array<readonly [P1, P2]> : never;
export declare type IndexingEnsurableMapArgument<K, V> = MapConstructorParameter<typeof Map, K, V>;
export declare type IndexingEnsurableMapCache<K, Indexed> = K extends {} ? EnsurableWeakMap<K & {}, Indexed> : EnsurableMap<K, Indexed>; /** @note AFAICS, getting `Indexed` inferred would be by `ReturnType<Indexing>`, which short circuits: “Type parameter defaults can only reference previously declared type parameters. ts(2744)”
    */
export declare type EnsurableMapCacheIndexing<K, Indexed> = (key: K) => Indexed;
export declare class IndexingEnsurableMap<K, V, Indexed = string> extends Map<Indexed, V> {
    indexing: EnsurableMapCacheIndexing<K, Indexed>;
    cache?: IndexingEnsurableMapCache<K, Indexed>;
    constructor(cache: boolean | IndexingEnsurableMapCache<K, Indexed>, /**		 sets `this.indexing` defaulting to this `||<Indexing><unknown>((k:any)=>''+k)` - note the "assertion"
        */ indexing?: EnsurableMapCacheIndexing<K, Indexed>, initials?: IndexingEnsurableMapArgument<Indexed, V>);
    protected _indexingEnsure(key: K): Indexed;
    ensureIndexed(key: Indexed, factory: (key: Indexed) => V): V;
    asyncEnsureIndexed(key: Indexed, factory: (key: Indexed) => Promise<V>): Promise<V>;
    ensure(key: K, factory: (key: K) => V): V;
    asyncEnsure(key: K, factory: (key: K) => Promise<V>): Promise<V>;
    indexingGet(key: K): V | undefined;
    indexingSet(key: K, value: V): this;
    getByIndexed(key: Indexed): V | undefined;
    setByIndexed(key: Indexed, value: V): this;
}
//# sourceMappingURL=ensurables.d.ts.map