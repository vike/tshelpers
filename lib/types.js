"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
; /** inspired from @thanks @see https://stackoverflow.com/questions/52760509/typescript-returntype-of-overloaded-function/52761156#52761156 original
    * @note n is atm capped at 8
    * @result [n] overload 1
    * @result [n-1] overload 2
    * @result [n-2] overload 3
    * @result ....
    * @result [3] overload n-2
    * @result [2] overload n-1
    * @result [1] overload n
    * @result [0] overloads 1...n as union
    */
function expanded(v) { return v; }
exports.expanded = expanded;
; /** Expands object types recursively
    * Renamed and reformated from @see https://stackoverflow.com/questions/57683303/how-can-i-see-the-full-expanded-contract-of-a-typescript-type/57683652#57683652 @thanks https://stackoverflow.com/users/2887218/jcalz
    */
function recursivelyExpanded(v) { return v; }
exports.recursivelyExpanded = recursivelyExpanded;
; /** renamed and formatted from @see https://stackoverflow.com/questions/53913496/how-to-type-constructor-as-well-as-constructor-argument-in-typescript/53943012#53943012 original
    */
const expecting_1 = require("./expectancy/expecting");
var test;
(function (test) {
    ;
    test.OmitFirst2 = () => {
        expecting_1.expectingEqual(['Rest'][0], 'Rest');
    };
})(test || (test = {}));
//# sourceMappingURL=types.js.map