"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//ported from bluelephant_tk.js 200629
;
function countMatches(expression, string) {
    if (!expression.global)
        throw new Error('countMatches(' + '): expression is not global');
    var c = 0;
    while (expression.exec(string) !== null)
        c++;
    return c;
}
exports.countMatches = countMatches;
;
function whereFailureIsAnOption(_) { try {
    return _();
}
catch (_) { } }
exports.whereFailureIsAnOption = whereFailureIsAnOption;
;
function whereFailureIsAnInstance(_) { try {
    return _();
}
catch (_) {
    return _;
} }
exports.whereFailureIsAnInstance = whereFailureIsAnInstance;
//# sourceMappingURL=functions.js.map