export declare function countMatches(expression: RegExp, string: string): number;
export declare function whereFailureIsAnOption<R>(_: () => R | undefined): R | undefined;
export declare function whereFailureIsAnInstance<R>(_: () => R): R | Error;
//# sourceMappingURL=functions.d.ts.map