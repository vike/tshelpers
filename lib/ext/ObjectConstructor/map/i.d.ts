export default function map<Source extends {
    [Key in keyof Source]: Source[Key];
} | Iterable<[keyof Source, Source[keyof Source]]>, Target extends {
    [Key in keyof Source]: TargetValue;
}, TargetValue>(from: Source, into: Target | null, mapper: <Key extends keyof typeof from>(val: (typeof from)[Key], key: Key, mappingFrom: typeof from, mappingInto: NonNullable<typeof into>) => TargetValue): Target;
//# sourceMappingURL=i.d.ts.map